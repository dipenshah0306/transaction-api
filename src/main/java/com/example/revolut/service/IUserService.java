package com.example.revolut.service;

import com.example.revolut.entity.User;
import com.google.inject.ImplementedBy;

/**
 * 
 * @author Dipen
 *
 */
@ImplementedBy(UserServiceImpl.class)
public interface IUserService extends IAbstractService<User, Long> {

	/**
	 * 
	 * @param userName
	 * @param firstName
	 * @param lastName
	 * @return
	 */
	public User createUserEntry(String userName, String firstName, String lastName);

	/**
	 * 
	 * @param userName
	 * @return
	 */
	public User getDetailsByUserName(String userName);
}
