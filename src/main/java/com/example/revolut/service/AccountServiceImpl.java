package com.example.revolut.service;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpStatus;

import com.example.revolut.bean.CreateAccountRequest;
import com.example.revolut.bean.CreateAccountResponse;
import com.example.revolut.dao.IAccountDao;
import com.example.revolut.entity.Account;
import com.example.revolut.entity.User;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;

/**
 * 
 * @author Dipen
 *
 */
@Singleton
@Transactional
public class AccountServiceImpl implements IAccountService {

	private static final Log _log = LogFactory.getLog(AccountServiceImpl.class);
	private static final String SAVINGS_TYPE_ACCOUNT = "Savings";

	@Inject
	private IAccountDao accountDao;

	@Inject
	private IUserService userService;

	@Inject
	private ITransactionHistoryService txnHistoryService;
	
	@Transactional
	@Override
	public Account add(Account t) {
		return accountDao.add(t);
	}

	@Transactional
	@Override
	public void delete(Account t) {
		accountDao.delete(t);
	}

	@Override
	public Account getById(Long t) {
		return accountDao.getById(t);
	}

	@Override
	public List<Account> getAll() {
		return accountDao.getAll();
	}

	@Transactional
	@Override
	public Account update(Account t) {
		return accountDao.update(t);
	}

	/**
	 * 
	 * @param userName
	 * @return
	 */
	@Transactional
	@Override
	public Account getAccountDetailsByUserName(String userName) {
		Account account = null;
		List<Account> accountList = accountDao.getAccountDetailsByUserName(userName);
		if (null != accountList && !accountList.isEmpty()) {
			account = accountList.get(0);
		}
		return account;
	}

	
	@Transactional
	@Override
	public User getUserDetilsByAccntId(Long accountId) {
		User user = null;
		List<User> userList = accountDao.getUserDetilsByAccntId(accountId);
		if (null != userList && !userList.isEmpty()) {
			user = userList.get(0);
		}
		return user;
	}
	
	/**
	 * Creating Account with Balance 10K
	 * 
	 * @param request
	 * @return
	 */
	@Transactional
	@Override
	public CreateAccountResponse createAccount(CreateAccountRequest request) {
		CreateAccountResponse response = new CreateAccountResponse();
		try {
			if (null != request) {
				User user = userService.createUserEntry(request.getUserName().toLowerCase(), request.getFirstName(),
						request.getLastName());
				if (null != user && null != user.getUserId()) {
					Account account = new Account();
					account.setAccountUser(user);
					account.setActive(Boolean.TRUE);
					account.setBalance(10000.00);
					account.setProofDetails(request.getProofDetails());
					account.setProofType(request.getProofType());
					account.setType(SAVINGS_TYPE_ACCOUNT);
					account = add(account);
					if (null != account && null != account.getAccountId()) {
						// TXN Table Entry
						txnHistoryService.createInitialEntry(account);
						// Sending Response
						response.setAccountId(account.getAccountId());
						response.setResponseMessage("Account Create Successfully!!");
						response.setStatus("SUCCESS");
						response.setResponseCode(HttpStatus.SC_OK);
					}
				} else {
					_log.info("Failed to create user entry");
				}
			} else {
				_log.info("Request is null");
				response.setResponseCode(HttpStatus.SC_PRECONDITION_FAILED);
				response.setResponseMessage("Request Invalid!!");
				response.setStatus("FAILED");
			}
		} catch (Exception e) {
			_log.error("Create Account Failed", e);
		}
		return response;
	}
	
	private Account createAccountEntry() {
		Account account = null;
		return account;
	}
	
	
	/**
	 * 
	 * @param account
	 * @param ledgerBalance
	 */
	@Transactional
	@Override
	public Account updateAccountBalance(String userName, Double amount, String txnType) {
		Account account = getAccountDetailsByUserName(userName);
		if(txnType.equals("Credit")) {			
			account.setBalance(account.getBalance() + amount);
		}else {
			account.setBalance(account.getBalance() - amount);
		}
		return accountDao.update(account);
	}
}
