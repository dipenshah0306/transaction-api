package com.example.revolut.service;

import java.util.List;

/**
 * 
 * @author 399581
 *
 * @param <T1>
 * @param <T2>
 */
public interface IAbstractService<T1, T2> {

	/**
	 * 
	 * @param t
	 * @return
	 */
	public T1 add(T1 t);

	/**
	 * 
	 * @param t
	 */
	public void delete(T1 t);

	/**
	 * 
	 * @param t
	 * @return
	 */
	public T1 getById(T2 t);

	/**
	 * 
	 * @return
	 */
	public List<T1> getAll();

	/**
	 * 
	 * @param t
	 * @return
	 */
	public T1 update(T1 t);
}