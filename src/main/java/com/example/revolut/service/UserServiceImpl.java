package com.example.revolut.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.example.revolut.dao.IUserDao;
import com.example.revolut.entity.User;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;

/**
 * 
 * @author Dipen
 *
 */
@Singleton
public class UserServiceImpl implements IUserService {

	private static final Log _log = LogFactory.getLog(UserServiceImpl.class);

	private String BUSINESS_TYPE_USER = "Business";

	@Inject
	private IUserDao userDao;

	@Transactional
	@Override
	public User add(User t) {
		User user = null;
		try {
			user = userDao.add(t);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return user;
	}

	@Transactional
	@Override
	public void delete(User t) {
		userDao.delete(t);
	}

	@Transactional
	@Override
	public User getById(Long t) {
		return userDao.getById(t);
	}

	@Transactional
	@Override
	public List<User> getAll() {
		return userDao.getAll();
	}

	@Transactional
	@Override
	public User update(User t) {
		return userDao.update(t);
	}

	@Override
	@Transactional
	public User getDetailsByUserName(String userName) {
		User user = null;
		List<User> userList = userDao.getUserDetailsByUserName(userName);
		if (null != userList && !userList.isEmpty()) {
			user = userList.get(0);
		}
		return user;
	}

	/**
	 * 
	 * @param userName
	 * @param firstName
	 * @param lastName
	 * @return
	 */
	@Override
	@Transactional
	public User createUserEntry(String userName, String firstName, String lastName) {
		User user = null;
		try {
			if (StringUtils.isNotBlank(userName)) {
				user = getDetailsByUserName(userName);
				if (null == user) {
					user = new User();
					user.setUserName(userName);
					user.setFirstName(firstName);
					user.setLastName(lastName);
					user.setUserType(BUSINESS_TYPE_USER);
					user = add(user);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}
}
