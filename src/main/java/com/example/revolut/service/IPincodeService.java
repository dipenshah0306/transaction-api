package com.example.revolut.service;

import java.util.List;

import com.example.revolut.entity.Pincode;
import com.example.revolut.vo.PincodeResponseVO;
import com.google.inject.ImplementedBy;

/**
 * 
 * @author 399581
 *
 */
@ImplementedBy(PincodeServiceImpl.class)
public interface IPincodeService extends IAbstractService<Pincode, Long> {

	/**
	 * 
	 * @param pinCode
	 * @return
	 */
	public List<Pincode> getPinCodeDetails(Long pinCode);
	
	/**
	 * 
	 * @param pinCode
	 * @return
	 */
	public PincodeResponseVO getPinCodeResponseDetails(Long pinCode);
}
