package com.example.revolut.service;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpStatus;

import com.example.revolut.bean.FundTransferRequest;
import com.example.revolut.bean.FundTransferResponse;
import com.example.revolut.dao.ITransactionHistoryDao;
import com.example.revolut.entity.Account;
import com.example.revolut.entity.TransactionHistory;
import com.example.revolut.entity.User;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;

/**
 * 
 * @author Dipen
 *
 */
@Singleton
@Transactional
public class TransactionHistoryServiceImpl implements ITransactionHistoryService {

	private static final Log _log = LogFactory.getLog(TransactionHistoryServiceImpl.class);

	@Inject
	private ITransactionHistoryDao transactionHistoryDao;

	@Inject
	private IUserService userService;

	@Inject
	private IAccountService accountService;

	@Override
	public TransactionHistory add(TransactionHistory t) {
		return transactionHistoryDao.add(t);
	}

	@Override
	public void delete(TransactionHistory t) {
		transactionHistoryDao.delete(t);
	}

	@Override
	public TransactionHistory getById(Long t) {
		return transactionHistoryDao.getById(t);
	}

	@Override
	public List<TransactionHistory> getAll() {
		return transactionHistoryDao.getAll();
	}

	@Override
	public TransactionHistory update(TransactionHistory t) {
		return transactionHistoryDao.update(t);
	}

	/**
	 * Initial Transaction History(Account Opening)
	 */
	@Override
	public TransactionHistory createInitialEntry(Account account) {
		return createCreditTransaction(10000D, account, account, "ACC Opening Balance");
	}

	/**
	 * 
	 * @param amount
	 * @param fromUser
	 * @param toUser
	 * @param txnMsg
	 * @return
	 */
	private TransactionHistory createTransactionHistory(Double amount, Account fromAccnt, Account toAccnt, String txnMsg,
			String txnType) {
		TransactionHistory transactionHistory = new TransactionHistory();
		transactionHistory.setAmount(amount);
		transactionHistory.setTxnFrom(fromAccnt);
		transactionHistory.setTxnTo(toAccnt);
		transactionHistory.setTxnMsg(txnMsg);
		transactionHistory.setTxnType(txnType);
		return transactionHistoryDao.add(transactionHistory);
	}

	public TransactionHistory createDebitTransaction(Double amount, Account fromAccnt, Account toAccnt, String txnMsg) {
		return createTransactionHistory(amount, fromAccnt, toAccnt, txnMsg, "Debit");
	}

	public TransactionHistory createCreditTransaction(Double amount, Account fromAccnt, Account toAccnt, String txnMsg) {
		return createTransactionHistory(amount, toAccnt, fromAccnt, txnMsg, "Credit");
	}

	@Override
	public FundTransferResponse tranferFunds(FundTransferRequest request) {
		FundTransferResponse response = new FundTransferResponse();
		if (null != request) {
			TransactionHistory txnHistory = transferFund(request.getFromAccountId(), request.getToAccountId(),
					request.getTxnAmount(), request.getTxnMsg());
			response.setResponseCode(HttpStatus.SC_OK);
			response.setResponseMessage("Funds Transferred Sucessfully");
			response.setStatus("SUCCESS");
			response.setTxnId(txnHistory.getTxnId());
		}
		return response;
	}

	@Transactional
	private void  updateAccountBalances(String userName, Double amount, String txnType) {
		accountService.updateAccountBalance(userName, amount, txnType);
	}	
	
	@Transactional
	public TransactionHistory transferFund(Long fromAccntId, Long toAccntId, Double amount, String message) {
		// Check 1 - Valid UserName
		TransactionHistory transactionHistory = null;
		if (null!=fromAccntId && null!=toAccntId) {
			User fromUser = accountService.getUserDetilsByAccntId(fromAccntId);
			User toUser = accountService.getUserDetilsByAccntId(toAccntId);
			if (null == fromUser) {
				// Invalid From User Details -- return
			}
			if (null == toUser) {
				// Invalid From User Details -- return
			}
			
			Account fromAccnt = accountService.getById(fromAccntId);
			Account toAccnt = accountService.getById(toAccntId);
			if (fromAccnt.getBalance() < amount) {
				// Low Balance to transfer amount
			}
			transactionHistory = createDebitTransaction(amount, fromAccnt, toAccnt,
					"Transferred Amount from: " + fromUser.getUserName() + " to: " + toUser.getUserName());
			createCreditTransaction(amount, fromAccnt, toAccnt,
					"Received Amount from: " + fromUser.getUserName() + " to: " + toUser.getUserName());
			/*
			 * accountService.updateAccountBalance(fromUser.getUserName(), amount, "Debit");
			 * accountService.updateAccountBalance(toUser.getUserName(), amount, "Credit");
			 */
			updateAccountBalances(fromUser.getUserName(), amount, "Debit");
			updateAccountBalances(toUser.getUserName(), amount, "Credit");
		} else {
			_log.info("Invalid User Account Details");
		}
		return transactionHistory;
	}
	
	
	
}
