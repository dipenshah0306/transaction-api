package com.example.revolut.service;

import com.example.revolut.bean.FundTransferRequest;
import com.example.revolut.bean.FundTransferResponse;
import com.example.revolut.entity.Account;
import com.example.revolut.entity.TransactionHistory;
import com.google.inject.ImplementedBy;

/**
 * 
 * @author Dipen
 *
 */
@ImplementedBy(TransactionHistoryServiceImpl.class)
public interface ITransactionHistoryService extends IAbstractService<TransactionHistory, Long>{

	/**
	 * 
	 * @param user
	 * @return
	 */
	public TransactionHistory createInitialEntry(Account account);
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	public FundTransferResponse tranferFunds(FundTransferRequest request); 
}
