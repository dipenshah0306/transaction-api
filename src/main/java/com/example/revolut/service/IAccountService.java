package com.example.revolut.service;

import com.example.revolut.bean.CreateAccountRequest;
import com.example.revolut.bean.CreateAccountResponse;
import com.example.revolut.entity.Account;
import com.example.revolut.entity.User;
import com.google.inject.ImplementedBy;

/**
 * 
 * @author Dipen
 *
 */
@ImplementedBy(AccountServiceImpl.class)
public interface IAccountService extends IAbstractService<Account, Long> {

	/**
	 * 
	 * @param request
	 * @return
	 */
	public CreateAccountResponse createAccount(CreateAccountRequest request);
	
	/**
	 * 
	 * @param userName
	 * @return
	 */
	public Account getAccountDetailsByUserName(String userName); 
	
	/**
	 * 
	 * @param accountId
	 * @return
	 */
	public User getUserDetilsByAccntId(Long accountId);
	
	/**
	 * 
	 * @param account
	 * @param amount
	 * @param txnType
	 * @return
	 */
	public Account updateAccountBalance(String userName, Double amount, String txnType);
}
