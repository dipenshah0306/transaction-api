package com.example.revolut.service;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.JsonGenerator.Feature;
import org.codehaus.jackson.map.ObjectMapper;

import com.example.revolut.dao.IPincodeDao;
import com.example.revolut.entity.Pincode;
import com.example.revolut.vo.PincodeResponseVO;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;

/**
 * 
 * @author 399581
 *
 */
@Singleton
public class PincodeServiceImpl implements IPincodeService {

	private static final Log _log = LogFactory.getLog(PincodeServiceImpl.class);
	
	@Inject
	private IPincodeDao pincodeDao;
	
	@Transactional
	@Override
	public Pincode add(Pincode t) {
		return pincodeDao.add(t);
	}

	@Transactional
	@Override
	public void delete(Pincode t) {
		pincodeDao.delete(t);
	}

	@Transactional
	@Override
	public Pincode getById(Long t) {
		return pincodeDao.getById(t);
	}

	@Transactional
	@Override
	public List<Pincode> getAll() {
		return pincodeDao.getAll();
	}

	@Transactional
	@Override
	public Pincode update(Pincode t) {
		return pincodeDao.update(t);
	}

	@Override
	public List<Pincode> getPinCodeDetails(Long pinCode) {
		return pincodeDao.getPinCodeDetails(pinCode);
	}
	
	/**
	 * 
	 * @param pinCode
	 * @return
	 */
	@Override
	public PincodeResponseVO getPinCodeResponseDetails(Long pinCode) {
		PincodeResponseVO pincodeResponseVO = new PincodeResponseVO();
		if (null != pinCode && pinCode != 0L) {
			if(String.valueOf(pinCode).length()==6){
				List<Pincode> pincodeList = getPinCodeDetails(pinCode);
				ObjectMapper objectMapper = new ObjectMapper();			
				objectMapper.configure(Feature.QUOTE_FIELD_NAMES, false);
				objectMapper.disable(org.codehaus.jackson.map.SerializationConfig.Feature.INDENT_OUTPUT);
				try {
					String responseDetails = objectMapper.writeValueAsString(pincodeList);
					pincodeResponseVO.setPincodeList(responseDetails);
				} catch (Exception e) {
					_log.error(e);
				} 
				if(null==pincodeList || pincodeList.isEmpty()){
					pincodeResponseVO.setStatus("Warning : No Record Exists on Mentioned Pincode");
					pincodeResponseVO.setStatusCode(0L);
				}else{				
					pincodeResponseVO.setStatus("Success");
					pincodeResponseVO.setStatusCode(1L);
				}	
			}else{
				pincodeResponseVO.setStatus("Failed : Please Enter Valid Pincode");
				pincodeResponseVO.setStatusCode(0L);
			}
		} else {
			pincodeResponseVO.setStatus("Failed");
			pincodeResponseVO.setStatusCode(0L);
		}
		return pincodeResponseVO;
	}
}
