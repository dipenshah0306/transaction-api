package com.example.revolut.ws;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.example.revolut.bean.FundTransferRequest;
import com.example.revolut.bean.FundTransferResponse;
import com.example.revolut.service.ITransactionHistoryService;
import com.google.inject.Inject;

@Path("/api/revolut/funds")
public class FundsTransferWSService {

	private static final Log _log = LogFactory.getLog(FundsTransferWSService.class);

	@Inject
	private ITransactionHistoryService txnHistoryService;

	/**
	 * 
	 * @param request
	 * @return
	 */
	@PUT
	@Path("/transfer")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public FundTransferResponse fundsTransfer(FundTransferRequest request) {
		return txnHistoryService.tranferFunds(request);
	}
}
