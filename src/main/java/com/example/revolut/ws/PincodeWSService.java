package com.example.revolut.ws;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.example.revolut.service.IPincodeService;
import com.google.inject.Inject;

@Path("/transaction")
@Produces("application/json")
public class PincodeWSService {

	private static final Log _log = LogFactory.getLog(PincodeWSService.class);
	
	@Inject
	private IPincodeService pincodeService;
	
	@GET
    @Path("/getDetails")
    public Response getUser(@QueryParam("pinCode") Long pinCode) {
		return Response.ok().entity(pincodeService.getPinCodeResponseDetails(pinCode)).build();
    }
}
