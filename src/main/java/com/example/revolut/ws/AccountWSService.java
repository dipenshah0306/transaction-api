package com.example.revolut.ws;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.example.revolut.bean.CreateAccountRequest;
import com.example.revolut.bean.CreateAccountResponse;
import com.example.revolut.service.IAccountService;
import com.google.inject.Inject;

@Path("/api/revolut/account")
public class AccountWSService {

	private static final Log _log = LogFactory.getLog(AccountWSService.class);

	@Inject
	private IAccountService accoutService;

	/**
	 * 
	 * @param request
	 * @return
	 */
	@POST
	@Path("/create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public CreateAccountResponse createAccount(CreateAccountRequest request) {
		return accoutService.createAccount(request);
	}
}
