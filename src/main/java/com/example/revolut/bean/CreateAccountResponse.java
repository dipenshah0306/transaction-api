package com.example.revolut.bean;

import java.io.Serializable;

/**
 * 
 * @author Dipen
 *
 */
public class CreateAccountResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3862791625932018483L;

	private int responseCode;
	private String responseMessage;
	private String status;
	private Long accountId;

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	@Override
	public String toString() {
		return "CreateAccountResponse [responseCode=" + responseCode + ", responseMessage=" + responseMessage
				+ ", status=" + status + ", accountId=" + accountId + "]";
	}
}
