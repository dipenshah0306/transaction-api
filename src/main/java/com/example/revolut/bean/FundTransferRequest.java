package com.example.revolut.bean;

import java.io.Serializable;

/**
 * 
 * @author Dipen
 *
 */
public class FundTransferRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4454400814156824315L;

	private Long fromAccountId;
	private Long toAccountId;
	private String txnMsg;
	private Double txnAmount;


	public Long getFromAccountId() {
		return fromAccountId;
	}

	public void setFromAccountId(Long fromAccountId) {
		this.fromAccountId = fromAccountId;
	}

	public Long getToAccountId() {
		return toAccountId;
	}

	public void setToAccountId(Long toAccountId) {
		this.toAccountId = toAccountId;
	}

	public String getTxnMsg() {
		return txnMsg;
	}

	public void setTxnMsg(String txnMsg) {
		this.txnMsg = txnMsg;
	}

	public Double getTxnAmount() {
		return txnAmount;
	}

	public void setTxnAmount(Double txnAmount) {
		this.txnAmount = txnAmount;
	}

	@Override
	public String toString() {
		return "FundTransferRequest [fromAccountId=" + fromAccountId + ", toAccountId=" + toAccountId + ", txnMsg="
				+ txnMsg + ", txnAmount=" + txnAmount + "]";
	}
}
