package com.example.revolut.bean;

import java.io.Serializable;

/**
 * 
 * @author Dipen
 *
 */
public class CreateAccountRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3597505890782812918L;

	private String userName;
	private String firstName;
	private String lastName;
	private String proofType;
	private String proofDetails;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getProofType() {
		return proofType;
	}

	public void setProofType(String proofType) {
		this.proofType = proofType;
	}

	public String getProofDetails() {
		return proofDetails;
	}

	public void setProofDetails(String proofDetails) {
		this.proofDetails = proofDetails;
	}

	@Override
	public String toString() {
		return "CreateAccountRequest [userName=" + userName + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", proofType=" + proofType + ", proofDetails=" + proofDetails + "]";
	}
}
