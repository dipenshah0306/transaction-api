package com.example.revolut.bean;

import java.io.Serializable;

/**
 * 
 * @author Dipen
 *
 */
public class FundTransferResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8190730885818185652L;

	private int responseCode;
	private String responseMessage;
	private String status;
	private Long txnId;

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getTxnId() {
		return txnId;
	}

	public void setTxnId(Long txnId) {
		this.txnId = txnId;
	}

	@Override
	public String toString() {
		return "FundTransferResponse [responseCode=" + responseCode + ", responseMessage=" + responseMessage
				+ ", status=" + status + ", txnId=" + txnId + "]";
	}
}
