package com.example.revolut;

import java.util.EnumSet;

import javax.servlet.DispatcherType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.jboss.resteasy.plugins.server.servlet.HttpServletDispatcher;

import com.example.revolut.web.GuiceContextListener;
import com.google.inject.servlet.GuiceFilter;

/**
 * 
 * @author Dipen
 *
 */
public class Bootstrap {

	private static final Log _log = LogFactory.getLog(Bootstrap.class);

	/**
	 * 
	 * @param s
	 * @throws Exception
	 */
	public static void main(String[] s) throws Exception {
		Server server = new Server(9090);
		ServletContextHandler handler = new ServletContextHandler(server, "/", ServletContextHandler.SESSIONS);
		handler.addFilter(GuiceFilter.class, "/*", EnumSet.allOf(DispatcherType.class));
		GuiceContextListener contextListener = new GuiceContextListener("transaction");
		handler.addEventListener(contextListener);
		handler.addServlet(HttpServletDispatcher.class, "/*");
		server.start();
	}
}
