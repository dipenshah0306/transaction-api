package com.example.revolut.dao;

import java.util.List;

import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.example.revolut.constant.CommonConstant;
import com.example.revolut.entity.User;

/**
 * 
 * @author Dipen
 *
 */
public class UserDaoImpl extends AbstractEntityDaoImpl<User, Long> implements IUserDao {

	private static final Log _log = LogFactory.getLog(UserDaoImpl.class);

	/**
	 * UserDaoImpl()
	 */
	public UserDaoImpl() {
		super(User.class);
	}

	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<User> getUserDetailsByUserName(String userName) {
		Query query = entityManager.createNamedQuery("getUserDetailsByUserName", User.class);
		query.setParameter(CommonConstant.USERNAME, userName.toLowerCase());
		List<User> userList = query.getResultList();
		return userList;
	}
}
