package com.example.revolut.dao;

import java.util.List;

import com.example.revolut.entity.Pincode;
import com.google.inject.ImplementedBy;

/**
 * 
 * @author 399581
 *
 */
@ImplementedBy(PincodeDaoImpl.class)
public interface IPincodeDao extends IAbstractEntityDao<Pincode, Long> {

	/**
	 * 
	 * @param pinCode
	 * @return
	 */
	public List<Pincode> getPinCodeDetails(Long pinCode);
}
