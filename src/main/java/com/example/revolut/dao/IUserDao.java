package com.example.revolut.dao;

import java.util.List;

import com.example.revolut.entity.User;
import com.google.inject.ImplementedBy;

/**
 * 
 * @author Dipen
 *
 */
@ImplementedBy(UserDaoImpl.class)
public interface IUserDao extends IAbstractEntityDao<User, Long> {

	/**
	 * 
	 * @param userName
	 * @return
	 */
	public List<User> getUserDetailsByUserName(String userName);
}
