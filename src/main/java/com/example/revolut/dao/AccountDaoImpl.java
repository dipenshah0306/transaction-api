package com.example.revolut.dao;

import java.util.List;

import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.example.revolut.entity.Account;
import com.example.revolut.entity.User;

/**
 * 
 * @author Dipen
 *
 */
public class AccountDaoImpl extends AbstractEntityDaoImpl<Account, Long> implements IAccountDao {

	private static final Log _log = LogFactory.getLog(AccountDaoImpl.class);
	private static final String USERNAME = "USERNAME";
	private static final String ACCOUNTID = "ACCOUNTID";

	/**
	 * AccountDaoImpl()
	 */
	public AccountDaoImpl() {
		super(Account.class);
	}

	/**
	 * 
	 * @param userName
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Account> getAccountDetailsByUserName(String userName) {
		Query query = entityManager.createNamedQuery("getAccountDetailsByUserName", Account.class);
		query.setParameter(USERNAME, userName);
		List<Account> accountList = query.getResultList();
		return accountList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<User> getUserDetilsByAccntId(Long accountId) {
		Query query = entityManager.createNamedQuery("getUserDetilsByAccntId", User.class);
		query.setParameter(ACCOUNTID, accountId);
		List<User> userList = query.getResultList();
		return userList;
	}
}
