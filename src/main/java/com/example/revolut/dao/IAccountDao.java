package com.example.revolut.dao;

import java.util.List;

import com.example.revolut.entity.Account;
import com.example.revolut.entity.User;
import com.google.inject.ImplementedBy;

/**
 * 
 * @author Dipen
 *
 */
@ImplementedBy(AccountDaoImpl.class)
public interface IAccountDao extends IAbstractEntityDao<Account, Long>{

	/**
	 * 
	 * @param userName
	 * @return
	 */
	public List<Account> getAccountDetailsByUserName(String userName);
	
	/**
	 * 
	 * @param accountId
	 * @return
	 */
	public List<User> getUserDetilsByAccntId(Long accountId);
}
