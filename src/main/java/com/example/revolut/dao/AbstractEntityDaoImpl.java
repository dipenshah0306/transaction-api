package com.example.revolut.dao;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 * 
 * @author Dipen
 *
 * @param <T1>
 * @param <ID>
 */
public abstract class AbstractEntityDaoImpl<T1, ID extends Serializable> implements IAbstractEntityDao<T1, ID> {

	@Inject
	protected EntityManager entityManager;

	private Class<T1> clazz;

	/**
	 * AbstractEntityDaoImpl()
	 */
	public AbstractEntityDaoImpl() {
	}

	/**
	 * 
	 * @param clazz
	 */
	public AbstractEntityDaoImpl(Class<T1> clazz) {
		this.clazz = clazz;
	}

	/**
	 * add
	 */
	@Override
	public T1 add(T1 t) {
		Boolean flag = false;
		try {
			if(!entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().begin();
				flag=true;
			}
			entityManager.persist(t);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(flag) {
			entityManager.getTransaction().commit();
		}
		return t;
	}

	/**
	 * delete
	 */
	@Override
	public void delete(T1 t) {
		entityManager.remove(t);
	}

	/**
	 * getById
	 */
	@Override
	public T1 getById(ID id) {
		return entityManager.find(clazz, id);
	}

	/**
	 * getAll()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<T1> getAll() {
		return entityManager.createQuery("From " + clazz.getName()).getResultList();
	}

	/**
	 * update
	 */
	@Override
	public T1 update(T1 t) {
		/*
		 * if(entityManager.getTransaction().isActive()) { t= entityManager.merge(t);
		 * entityManager.getTransaction().commit(); }else {
		 * entityManager.getTransaction().begin(); t=entityManager.merge(t);
		 * entityManager.getTransaction().commit(); }
		 */
		Boolean flag = false;
		try {
			if(!entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().begin();
				flag=true;
			}
			entityManager.merge(t);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(flag) {
			entityManager.getTransaction().commit();
		}
		return t;
	}
}
