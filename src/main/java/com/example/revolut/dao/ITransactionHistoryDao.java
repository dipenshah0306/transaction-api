package com.example.revolut.dao;

import com.example.revolut.entity.TransactionHistory;
import com.google.inject.ImplementedBy;

/**
 * 
 * @author Dipen
 *
 */
@ImplementedBy(TransactionHistoryDaoImpl.class)
public interface ITransactionHistoryDao extends IAbstractEntityDao<TransactionHistory, Long> {

}
