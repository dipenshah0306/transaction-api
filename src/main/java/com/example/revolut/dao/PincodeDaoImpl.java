package com.example.revolut.dao;

import java.util.List;

import javax.persistence.Query;

import com.example.revolut.entity.Pincode;

/**
 * 
 * @author 399581
 *
 */
public class PincodeDaoImpl extends AbstractEntityDaoImpl<Pincode, Long> implements IPincodeDao {

	/**
	 * PincodeDaoImpl()
	 */
	public PincodeDaoImpl() {
		super(Pincode.class);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Pincode> getPinCodeDetails(Long pinCode) {
		Query query = entityManager.createNamedQuery("getPinCodeDetails", Pincode.class);
		query.setParameter("PINCODE", pinCode);
		List<Pincode> pincodeList = query.getResultList();
		return pincodeList;
	}

}
