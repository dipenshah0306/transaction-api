package com.example.revolut.dao;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.example.revolut.entity.TransactionHistory;

/**
 * 
 * @author Dipen
 *
 */
public class TransactionHistoryDaoImpl extends AbstractEntityDaoImpl<TransactionHistory, Long>
		implements ITransactionHistoryDao {

	private static final Log _log = LogFactory.getLog(TransactionHistoryDaoImpl.class);
	
	/**
	 * TransactionHistoryDaoImpl()
	 */
	public TransactionHistoryDaoImpl() {
		super(TransactionHistory.class);
	}
}
