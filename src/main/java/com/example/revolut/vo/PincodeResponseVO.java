package com.example.revolut.vo;

import java.io.Serializable;

/**
 * 
 * @author 399581
 *
 */
public class PincodeResponseVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6949817148294520789L;

	private Long statusCode;
	private String status;
	private String pincodeList;

	public Long getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Long statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPincodeList() {
		return pincodeList;
	}

	public void setPincodeList(String pincodeList) {
		this.pincodeList = pincodeList;
	}
}
