package com.example.revolut.modules;

import com.google.inject.persist.PersistService;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * 
 * @author 399581
 *
 */
@Singleton
public class JpaInitializer {

	@Inject
	public JpaInitializer(final PersistService service) {
		service.start();
	}
}
