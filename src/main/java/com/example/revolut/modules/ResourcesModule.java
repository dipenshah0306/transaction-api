package com.example.revolut.modules;

import com.example.revolut.ws.AccountWSService;
import com.example.revolut.ws.FundsTransferWSService;
import com.example.revolut.ws.PincodeWSService;
import com.google.inject.AbstractModule;

/**
 * 
 * @author Dipen
 *
 */
public class ResourcesModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(PincodeWSService.class);
		bind(AccountWSService.class);
		bind(FundsTransferWSService.class);
	}
}
