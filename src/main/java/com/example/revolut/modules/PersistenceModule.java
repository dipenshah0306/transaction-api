package com.example.revolut.modules;

import com.example.revolut.dao.IAbstractEntityDao;
import com.example.revolut.dao.IAccountDao;
import com.example.revolut.dao.ITransactionHistoryDao;
import com.example.revolut.dao.IUserDao;
import com.example.revolut.entity.Account;
import com.example.revolut.entity.TransactionHistory;
import com.example.revolut.entity.User;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.google.inject.persist.jpa.JpaPersistModule;

/**
 * 
 * @author Dipen
 *
 */
public class PersistenceModule extends AbstractModule {

	private final String persistenceUnit;

	public PersistenceModule(String persistenceUnit) {
		this.persistenceUnit = persistenceUnit;
	}

	@Override
	protected void configure() {
		install(new JpaPersistModule(persistenceUnit));
		bind(JpaInitializer.class).asEagerSingleton();
		bind(new TypeLiteral<IAbstractEntityDao<User, Long>>() {
		}).to(new TypeLiteral<IUserDao>() {
		});
		bind(new TypeLiteral<IAbstractEntityDao<Account, Long>>() {
		}).to(new TypeLiteral<IAccountDao>() {
		});
		bind(new TypeLiteral<IAbstractEntityDao<TransactionHistory, Long>>() {
		}).to(new TypeLiteral<ITransactionHistoryDao>() {
		});
	}
}
