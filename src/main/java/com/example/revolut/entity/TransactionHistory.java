package com.example.revolut.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Table(name = "txn_history")
@Entity
//@NamedQueries({ @NamedQuery(name = "getPinCodeDetails", query = "From Pincode s where s.pincode = :PINCODE "), })
public class TransactionHistory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5930859032241009154L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long txnId;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "from_account_id", referencedColumnName = "id", nullable = false)
	private Account txnFrom;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "to_account_id", referencedColumnName = "id", nullable = false)
	private Account txnTo;

	@Column(name = "amount")
	private Double amount;

	@Column(name = "message")
	private String txnMsg;

	@Column(name = "txn_type")
	private String txnType;

	public Long getTxnId() {
		return txnId;
	}

	public void setTxnId(Long txnId) {
		this.txnId = txnId;
	}

	public Account getTxnFrom() {
		return txnFrom;
	}

	public void setTxnFrom(Account txnFrom) {
		this.txnFrom = txnFrom;
	}

	public Account getTxnTo() {
		return txnTo;
	}

	public void setTxnTo(Account txnTo) {
		this.txnTo = txnTo;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getTxnMsg() {
		return txnMsg;
	}

	public void setTxnMsg(String txnMsg) {
		this.txnMsg = txnMsg;
	}

	public String getTxnType() {
		return txnType;
	}

	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}

	@Override
	public String toString() {
		return "TransactionHistory [txnId=" + txnId + ", txnFrom=" + txnFrom + ", txnTo=" + txnTo + ", amount=" + amount
				+ ", txnMsg=" + txnMsg + "]";
	}
}
