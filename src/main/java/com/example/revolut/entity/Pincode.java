package com.example.revolut.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import net.karneim.pojobuilder.GeneratePojoBuilder;

/**
 * 
 * @author 399581
 *
 */
@Table(name = "mst_pincode_details")
@Entity
@Getter
@Setter
@EqualsAndHashCode(of = { "id" })
@GeneratePojoBuilder(withCopyMethod = true)
@NamedQueries({ @NamedQuery(name = "getPinCodeDetails", query = "From Pincode s where s.pincode = :PINCODE "), })
public class Pincode implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6519973498711393991L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@NotNull
	@Column(name = "pincode", nullable = false)
	private Long pincode;

	@Column(name = "post_office_name")
	private String postOfficeName;

	@Column(name = "division_name")
	private String divisionName;

	@Column(name = "region_name")
	private String regionName;

	@Column(name = "circle_name")
	private String circleName;

	@Column(name = "taluka_name")
	private String talukaName;

	@Column(name = "district_name")
	private String districtName;

	@Column(name = "state_name")
	private String stateName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPincode() {
		return pincode;
	}

	public void setPincode(Long pincode) {
		this.pincode = pincode;
	}

	public String getPostOfficeName() {
		return postOfficeName;
	}

	public void setPostOfficeName(String postOfficeName) {
		this.postOfficeName = postOfficeName;
	}

	public String getDivisionName() {
		return divisionName;
	}

	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public String getCircleName() {
		return circleName;
	}

	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}

	public String getTalukaName() {
		return talukaName;
	}

	public void setTalukaName(String talukaName) {
		this.talukaName = talukaName;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
}
