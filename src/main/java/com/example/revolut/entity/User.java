package com.example.revolut.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * 
 * @author Dipen
 *
 */
@Table(name = "mst_user_details")
@Entity
@NamedQueries({ @NamedQuery(name = "getUserDetailsByUserName", query = "From User s where s.userName = :USERNAME"), })
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1958089664448423393L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	private Long userId;

	@NotNull
	@Column(name = "user_name", nullable = false, unique = true)
	private String userName;

	@Column(name = "user_fname")
	private String firstName;

	@Column(name = "user_lname")
	private String lastName;

	@Column(name = "user_type")
	private String userType;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", userName=" + userName + ", firstName=" + firstName + ", lastName="
				+ lastName + ", userType=" + userType + "]";
	}
}
