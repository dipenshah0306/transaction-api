package com.example.revolut.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * 
 * @author Dipen
 *
 */
@Table(name = "mst_account_details")
@Entity
@NamedQueries({
		@NamedQuery(name = "getAccountDetailsByUserName", query = "From Account s where s.accountUser.userName = :USERNAME"),
		@NamedQuery(name = "getUserDetilsByAccntId", query = "Select s.accountUser From Account s where s.accountId= :ACCOUNTID"),})
public class Account implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6311250421905284631L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long accountId;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
	private User accountUser;

	@Column(name = "balance")
	private Double balance;

	@Column(name = "type")
	private String type;

	@Column(name = "proof_type")
	private String proofType;

	@Column(name = "proof_details")
	private String proofDetails;

	@Column(name = "active")
	private Boolean active;

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public User getAccountUser() {
		return accountUser;
	}

	public void setAccountUser(User accountUser) {
		this.accountUser = accountUser;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getProofType() {
		return proofType;
	}

	public void setProofType(String proofType) {
		this.proofType = proofType;
	}

	public String getProofDetails() {
		return proofDetails;
	}

	public void setProofDetails(String proofDetails) {
		this.proofDetails = proofDetails;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	@Override
	public String toString() {
		return "Account [accountId=" + accountId + ", accountUser=" + accountUser + ", balance=" + balance + ", type="
				+ type + ", proofType=" + proofType + ", proofDetails=" + proofDetails + ", active=" + active + "]";
	}
}
