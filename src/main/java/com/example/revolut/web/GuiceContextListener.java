package com.example.revolut.web;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;

import org.jboss.resteasy.plugins.guice.GuiceResteasyBootstrapServletContextListener;

import com.example.revolut.modules.PersistenceModule;
import com.example.revolut.modules.ResourcesModule;
import com.google.inject.Module;

/**
 * 
 * @author 399581
 *
 */
public class GuiceContextListener extends GuiceResteasyBootstrapServletContextListener {

	private final String persistenceUnit;

	public GuiceContextListener(String persistenceUnit) {
		this.persistenceUnit = persistenceUnit;
	}

	/**
	 * Override this method to instantiate your
	 * {@link com.google.inject.Module}s yourself.
	 *
	 * @param context
	 * @return
	 */
	protected List<? extends Module> getModules(final ServletContext context) {
		final List<Module> result = new ArrayList<Module>();
		result.add(new PersistenceModule(persistenceUnit));
		result.add(new ResourcesModule());
		return result;
	}
}
